/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.accesBD;

import gestionDT.modele.entites.DemandeTravaux;
import iutlr.dutinfo.bd.AccesBD;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author mhverron
 */
public class EnregistrementDonnees {

    private AccesBD monAccesBD;

    public EnregistrementDonnees() {
        this.monAccesBD = new AccesBD();
    }

    public int enregistrerDemandeTravaux(DemandeTravaux uneDT) {
        String sql = "insert into Demande_Travaux(numDT, description, typeDemande, emetteur) "
                + "values ("
                + "(select NVL(max(numDT), 0)+1 from Demande_Travaux)" + ","
                + "'" + uneDT.getDescription() + "',"
                + uneDT.getType().getIdType() + ","
                + uneDT.getEmetteur().getIdEmploye()
                + ")";
        int nb = 0;
        try {
            nb = this.monAccesBD.mettreAjourBase(sql);
            if (nb == 1){
                this.mettreAjourNumDT(uneDT);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return nb;
    }

    
    private void mettreAjourNumDT(DemandeTravaux uneDT) {
        try{
            List<List<String>> resultats = this.monAccesBD.interrogerBase("select max(numDT) from Demande_Travaux");
            if (!resultats.isEmpty()) {
                List<String> row = resultats.get(0);
                uneDT.setNumDT(Integer.parseInt(row.get(0)));
            }
        } catch (SQLException ex) {
            System.out.println("Erreur récupération numDT");
        }
    }

    /**
     * Permet l'annulation d'une DT
     * @param laDT
     * @return 
     */
    public int annulerDT(DemandeTravaux laDT) {
        try{
            int n = this.monAccesBD.mettreAjourBase("delete from Demande_Travaux where numDT =" + laDT.getNumDT());
            return n;
        }
        catch(SQLException ex){
            System.out.println("Erreur suppression DT");
            return -1;
        }
    }
    
}
