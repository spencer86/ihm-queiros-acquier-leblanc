/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.accesBD;


import gestionDT.modele.entites.Agence;
import gestionDT.modele.entites.Employe;
import gestionDT.modele.entites.EtatDemande;
import gestionDT.modele.entites.Service;
import gestionDT.modele.entites.Technicien;
import gestionDT.modele.entites.TypeDemande;
import gestionDT.modele.entites.DemandeTravaux;
import iutlr.dutinfo.bd.AccesBD;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mhverron
 */
public class RecupererDonneesInitiales {

    private AccesBD monAccesBD;
    private Map<Integer, Service> lesServices;
    private List<Integer> lesIdResponsables;
    private Map<Integer, Agence> lesAgences;
    private Map<Integer, TypeDemande> lesTypesDemande;

    public RecupererDonneesInitiales() {
        this.monAccesBD = new AccesBD();
        this.lesAgences = new HashMap<>();
        this.lesServices = new HashMap<>();
        this.lesTypesDemande = new HashMap<>();
        this.lesIdResponsables = new ArrayList<>();
    }

    public Agence recupererAgence(int idAgence) {
        if (this.lesAgences.containsKey(idAgence)) {
            return this.lesAgences.get(idAgence);
        }
        Agence uneAgence = null;
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select nomAgence from Agence where idAgence=" + idAgence);
            if (!resultats.isEmpty()) {
                List<String> row = resultats.get(0);
                uneAgence = new Agence(idAgence, row.get(0));
                this.lesAgences.put(idAgence, uneAgence);
            }
        } catch (SQLException e) {
            System.out.println("Erreur recup agence " + idAgence);
        }
        return uneAgence;
    }

    public Service recupererService(int idService) {
        if (this.lesServices.containsKey(idService)) {
            return this.lesServices.get(idService);
        }
        Service unService = null;
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select nomService, responsable from Service where idService=" + idService);
            if (!resultats.isEmpty()) {
                List<String> row = resultats.get(0);
                unService = new Service(idService, row.get(0));
                this.lesServices.put(idService, unService);
                this.lesIdResponsables.add(Integer.parseInt(row.get(1)));
            }
        } catch (SQLException e) {
            System.out.println("Erreur recup service " + idService);
        }
        return unService;
    }

    public Employe recupererEmployeConnecte() {
        Employe unEmploye = null;
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select idEmploye, nomEmploye, telBureau, mail, technicien, service, agence from Employe where idEmploye=3");
            if (!resultats.isEmpty()) {
                List<String> row = resultats.get(0);
                int idEmploye = Integer.parseInt(row.get(0));
                int idService = Integer.parseInt(row.get(5));
                int idAgence = Integer.parseInt(row.get(6));
                String technicien = row.get(4);
                if ("N".equals(technicien)) {
                    unEmploye = new Employe(idEmploye, row.get(1), row.get(2), row.get(3), this.recupererAgence(idAgence), this.recupererService(idService));
                } else {
                    unEmploye = new Technicien(idEmploye, row.get(1), row.get(2), row.get(3), this.recupererAgence(idAgence), this.recupererService(idService));
                }
                if (this.lesIdResponsables.contains(idEmploye)) {
                    this.lesServices.get(idService).setResponsable(unEmploye);
                }
                this.recupererDemandesPostees(unEmploye);
            }
        } catch (SQLException e) {
            System.out.println("Erreur recup employe connecte");
        }
        return unEmploye;
    }

    public List<TypeDemande> recupererTypesDemande() {
        List<TypeDemande> lesTypes = new ArrayList<>();
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select idTypeDemande, libelle from Type_Demande");
            for (List<String> row : resultats) {
                TypeDemande unType = new TypeDemande(Integer.parseInt(row.get(0)), row.get(1));
                lesTypes.add(unType);
                this.lesTypesDemande.put(unType.getIdType(), unType);
            }
        } catch (SQLException e) {
            System.out.println("Erreur recup types demande");
        }
        return lesTypes;
    }

    public void recupererDemandesPostees(Employe unEmploye) {
        List<DemandeTravaux> lesDT = new ArrayList<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select numDT, dateDemande, description, etatDemande, dateCloture, datePriseEnCharge, priorite, typeDemande from Demande_Travaux where emetteur=" + unEmploye.getIdEmploye());
            for (List<String> row : resultats) {
                DemandeTravaux uneDT = new DemandeTravaux(Integer.parseInt(row.get(0)), df.parse(row.get(1)), row.get(2), EtatDemande.valueOf(row.get(3).toUpperCase()), unEmploye, this.lesTypesDemande.get(Integer.parseInt(row.get(7))));
                if (row.get(5) != null) {
                    uneDT.setDatePriseEnCharge(df.parse(row.get(5)));
                }
                if (row.get(4) != null) {
                    uneDT.setDateCloture(df.parse(row.get(4)));
                }
                if (row.get(6) != null) {
                    uneDT.setPriorite(Integer.parseInt(row.get(6)));
                }
                lesDT.add(uneDT);
            }        
            unEmploye.setDT(lesDT);
        } catch (SQLException e) {
            System.out.println("Erreur recup demandes de travaux de l'employe " + unEmploye.getNom());
        } catch (ParseException ex) {
            System.out.println("Erreur recup demandes de travaux de l'employe " + unEmploye.getNom());            
        }

    }
}
