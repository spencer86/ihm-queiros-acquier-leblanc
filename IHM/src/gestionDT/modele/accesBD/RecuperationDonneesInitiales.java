/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.accesBD;

import gestionDT.modele.entites.Employe;
import gestionDT.modele.entites.TypeDemande;
import gestionDT.modele.entites.Agence;
import gestionDT.modele.entites.Service;
import iutlr.dutinfo.bd.AccesBD;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author mhverron
 */
public class RecuperationDonneesInitiales {

    private AccesBD monAccesBD;
    private Map<Integer, Service> lesServices;
    private Set<Integer> lesIdResponsables;
    private Map<Integer, Agence> lesAgences;

    public RecuperationDonneesInitiales() {
        this.monAccesBD = new AccesBD();
        this.lesAgences = new HashMap<>();
        this.lesServices = new HashMap<>();
        this.lesIdResponsables = new HashSet<>();
    }

    public Agence recupererAgence(int idAgence) {
        if (this.lesAgences.containsKey(idAgence)) {
            return this.lesAgences.get(idAgence);
        }
        Agence uneAgence = null;
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select nomAgence from Agence where idAgence=" + idAgence);
            List<String> row = resultats.get(0);
            uneAgence = new Agence(idAgence, row.get(0));
            this.lesAgences.put(idAgence, uneAgence);
        } catch (SQLException e) {
            System.out.println("Erreur recup agence " + idAgence);
        }
        return uneAgence;
    }


    public Service recupererService(int idService) {
        if (this.lesServices.containsKey(idService)) {
            return this.lesServices.get(idService);
        }
        Service unService = null;
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select nomService, responsable from Service where idService=" + idService);
            List<String> row = resultats.get(0);
            unService = new Service(idService, row.get(0));
            this.lesServices.put(idService, unService);
            this.lesIdResponsables.add(Integer.parseInt(row.get(1)));
        } catch (SQLException e) {
            System.out.println("Erreur recup service " + idService);
        }
        return unService;
    }

    public Employe recupererEmployeConnecte() {
        Employe unEmploye = null;
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select idEmploye, nomEmploye, telBureau, mail, service, agence from Employe where idEmploye=3");
            List<String> row = resultats.get(0);
            int idEmploye = Integer.parseInt(row.get(0));
            int idService = Integer.parseInt(row.get(4));
            int idAgence = Integer.parseInt(row.get(5));
            unEmploye = new Employe(idEmploye, row.get(1), row.get(2), row.get(3), this.recupererAgence(idAgence), this.recupererService(idService));
            if (this.lesIdResponsables.contains(idEmploye)) {
                this.lesServices.get(idService).setResponsable(unEmploye);
            }
        } catch (SQLException e) {
            System.out.println("Erreur recup employe connecte");
        }
        return unEmploye;
    }

    public List<TypeDemande> recupererTypesDemande() {
        List<TypeDemande> lesTypes = new ArrayList<>();
        try {
            List<List<String>> resultats = monAccesBD.interrogerBase("select idTypeDemande, libelle from Type_Demande");
            for (List<String> row : resultats) {
                lesTypes.add(new TypeDemande(Integer.parseInt(row.get(0)), row.get(1)));
            }
        } catch (SQLException e) {
            System.out.println("Erreur recup types demande");
        }
        return lesTypes;
    }
}
