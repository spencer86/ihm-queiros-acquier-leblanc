/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mhverron
 */
public class DemandeTravaux {
    
    private int numDT;
    private Date dateDemande;
    private String description;
    private EtatDemande etat;
    private Date dateCloture;
    private int priorite;
    private Date datePriseEnCharge;
    private Employe emetteur;
    private TypeDemande monType;
    private List<Commentaire> commentaires;

    /**
     * Constructeur de DemandeTravaux
     * @param description
     * @param emetteur
     * @param unType 
     */
    public DemandeTravaux(String description, Employe emetteur, TypeDemande unType) {
        this.description = description;
        this.emetteur = emetteur;
        this.monType = unType;
        this.dateDemande = new Date();
        this.etat = EtatDemande.POSTEE;
        this.commentaires = new ArrayList<>();
    }

    /**
     * Autre constructeur de DemandeTravaux
     * @param numDT
     * @param dateDemande
     * @param description
     * @param etat
     * @param dateCloture
     * @param priorite
     * @param datePriseEnCharge 
     */
    public DemandeTravaux(int numDT, Date dateDemande, String description, EtatDemande etat, Date dateCloture, int priorite, Date datePriseEnCharge) {
        this.numDT = numDT;
        this.dateDemande = dateDemande;
        this.description = description;
        this.etat = etat;
        this.dateCloture = dateCloture;
        this.priorite = priorite;
        this.datePriseEnCharge = datePriseEnCharge;
        this.commentaires = new ArrayList<>();
    }

    /**
     * Autre getter sur DemandeTravaux
     * @param numDT
     * @param dateDemande
     * @param description
     * @param etat
     * @param emetteur
     * @param monType 
     */
    public DemandeTravaux(int numDT, Date dateDemande, String description, EtatDemande etat, Employe emetteur, TypeDemande monType) {
        this.numDT = numDT;
        this.dateDemande = dateDemande;
        this.description = description;
        this.etat = etat;
        this.emetteur = emetteur;
        this.monType = monType;
        this.commentaires = new ArrayList<>();
    }

    /**
     * Setter sur numDT
     * @param numDT 
     */
    public void setNumDT(int numDT) {
        this.numDT = numDT;
    }

    /**
     * Setter sur dateCloture
     * @param dateCloture 
     */
    public void setDateCloture(Date dateCloture) {
        this.dateCloture = dateCloture;
    }

    /**
     * Setter sur priorite
     * @param priorite 
     */
    public void setPriorite(int priorite) {
        this.priorite = priorite;
    }

    /**
     * Setter sur datePriseEnCharge
     * @param datePriseEnCharge 
     */
    public void setDatePriseEnCharge(Date datePriseEnCharge) {
        this.datePriseEnCharge = datePriseEnCharge;
    }

    /**
     * Setter sur emetteur
     * @param emetteur 
     */
    public void setEmetteur(Employe emetteur) {
        this.emetteur = emetteur;
    }

    /**
     * Setter sur type de TypeDemande
     * @param unType 
     */
    public void setType(TypeDemande unType) {
        this.monType = unType;
    }

    /**
     * Getter sur numDT
     * @return 
     */
    public int getNumDT() {
        return numDT;
    }

    /**
     * Getter sur dateDemande
     * @return 
     */
    public Date getDateDemande() {
        return dateDemande;
    }

    /**
     * Getter sur description
     * @return 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Getter sur etat
     * @return 
     */
    public EtatDemande getEtat() {
        return etat;
    }

    /**
     * Getter sur dateCloture
     * @return 
     */
    public Date getDateCloture() {
        return dateCloture;
    }

    /**
     * Getter sur priorite
     * @return 
     */
    public int getPriorite() {
        return priorite;
    }

    /**
     * Getter sur datePriseEnCharge
     * @return 
     */
    public Date getDatePriseEnCharge() {
        return datePriseEnCharge;
    }

    /**
     * Getter sur emetteur
     * @return 
     */
    public Employe getEmetteur() {
        return emetteur;
    }

    /**
     * Getter sur type de TypeDemande
     * @return 
     */
    public TypeDemande getType() {
        return monType;
    }
    
    /**
     * Permet d'ajouter un commentaire
     * @param unCommentaire 
     */
    public void ajouterCommentaire(Commentaire unCommentaire){
        this.commentaires.add(unCommentaire);
    }
    
}
