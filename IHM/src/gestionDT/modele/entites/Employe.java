/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mhverron
 */
public class Employe {
    
    protected int idEmploye;
    protected String nom;
    protected String telBureau;
    protected String mail;
    protected Agence monAgence;
    protected Service monService;
    private List<DemandeTravaux> mesDT;

    public Employe(int idEmploye, String nom, String telBureau, String mail) {
        this.idEmploye = idEmploye;
        this.nom = nom;
        this.telBureau = telBureau;
        this.mail = mail;
        this.mesDT = new ArrayList<>();
    }

    public Employe(int idEmploye, String nom, String telBureau, String mail, Agence uneAgence, Service unService) {
        this.idEmploye = idEmploye;
        this.nom = nom;
        this.telBureau = telBureau;
        this.mail = mail;
        this.monAgence = uneAgence;
        this.monService = unService;
        this.mesDT = new ArrayList<>();
    }

    public void setAgence(Agence uneAgence) {
        this.monAgence = uneAgence;
    }

    public void setService(Service unService) {
        this.monService = unService;
    }

    public int getIdEmploye() {
        return idEmploye;
    }

    public String getNom() {
        return nom;
    }

    public String getTelBureau() {
        return telBureau;
    }

    public String getMail() {
        return mail;
    }

    public Agence getAgence() {
        return monAgence;
    }

    public Service getService() {
        return monService;
    }
    /**
     * Permet d'ajouter une DT
     * @param uneDT 
     */
    public void ajouterDT(DemandeTravaux uneDT){
        this.mesDT.add(uneDT);
    }

    public List<DemandeTravaux> getDT() {
        return mesDT;
    }

    public void setDT(List<DemandeTravaux> mesDT) {
        this.mesDT = mesDT;
    }
    
    
    
}
