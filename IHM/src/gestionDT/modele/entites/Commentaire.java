/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

import java.util.Date;

/**
 *
 * @author mhverron
 */
public class Commentaire {
    
    private int idComm;
    private Date dateComm;
    private String libelle;
    private Technicien emetteur;
    private DemandeTravaux dt;

    /**
     * Constructeur de Commentaire
     * @param dateComm
     * @param libelle
     * @param emetteur
     * @param dt 
     */
    public Commentaire(Date dateComm, String libelle, Technicien emetteur, DemandeTravaux dt) {
        this.dateComm = dateComm;
        this.libelle = libelle;
        this.emetteur = emetteur;
        this.dt = dt;
    }
    /**
     * Autre constructeur de Commentaire
     * @param dateComm
     * @param libelle 
     */
    public Commentaire(Date dateComm, String libelle) {
        this.dateComm = dateComm;
        this.libelle = libelle;
    }

    /**
     * Setter sur emetteur
     * @param emetteur 
     */
    public void setEmetteur(Technicien emetteur) {
        this.emetteur = emetteur;
    }

    /**
     * Setter sur dt
     * @param dt 
     */
    public void setDt(DemandeTravaux dt) {
        this.dt = dt;
    }

    /**
     * Setter sur idComm
     * @param idComm 
     */
    public void setIdComm(int idComm) {
        this.idComm = idComm;
    }

    /**
     * Getter sur idComm
     * @return 
     */
    public int getIdComm() {
        return idComm;
    }

    /**
     * Getter sur dateComm
     * @return 
     */
    public Date getDateComm() {
        return dateComm;
    }

    /**
     * Getter sur libelle
     * @return 
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Getter sur emetteur
     * @return 
     */
    public Technicien getEmetteur() {
        return emetteur;
    }

    /**
     * Getter sur dt
     * @return 
     */
    public DemandeTravaux getDt() {
        return dt;
    }
    
    
}
