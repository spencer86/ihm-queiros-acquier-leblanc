/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

/**
 *
 * @author mhverron
 */
public class Agence {
    
    private int idAgence;
    private String nom;

    /**
     * Constructeur Agence
     * @param idAgence
     * @param nom 
     */
    public Agence(int idAgence, String nom) {
        this.idAgence = idAgence;
        this.nom = nom;
    }
    /**
     * Getter sur idAgence
     * @return 
     */
    public int getIdAgence() {
        return idAgence;
    }
    /**
     * Getter sur nom
     * @return 
     */
    public String getNom() {
        return nom;
    }
    
    
    
}
