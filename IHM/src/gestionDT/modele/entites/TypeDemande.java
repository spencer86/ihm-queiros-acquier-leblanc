/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

/**
 *
 * @author mhverron
 */
public class TypeDemande {
    
    private int idType;
    private String libelle;

    public TypeDemande(int idType, String libelle) {
        this.idType = idType;
        this.libelle = libelle;
    }

    public int getIdType() {
        return idType;
    }

    public String getLibelle() {
        return libelle;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
    
    
    
}
