/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

import java.util.Date;

/**
 *
 * @author mhverron
 */
public class Affectation {
    
    private int idAffectation;
    private Date dateAffectation;
    private Date datePriseEnCharge;
    private Technicien technicienAffecte;
    private DemandeTravaux dt;

    public Affectation(Technicien technicienAffecte, DemandeTravaux dt) {
        this.technicienAffecte = technicienAffecte;
        this.dt = dt;
    }

    /**
     * 
     * @param idAffectation
     * @param dateAffectation
     * @param datePriseEnCharge 
     */
    public Affectation(int idAffectation, Date dateAffectation, Date datePriseEnCharge) {
        this.idAffectation = idAffectation;
        this.dateAffectation = dateAffectation;
        this.datePriseEnCharge = datePriseEnCharge;
    }
    /**
     * Setter sur idAffectation
     * @param idAffectation 
     */
    public void setIdAffectation(int idAffectation) {
        this.idAffectation = idAffectation;
    }
    /**
     * Setter sur datePriseEnCharge
     * @param datePriseEnCharge 
     */
    public void setDatePriseEnCharge(Date datePriseEnCharge) {
        this.datePriseEnCharge = datePriseEnCharge;
    }
    /**
     * Setter sur technicienAffecte
     * @param technicienAffecte 
     */
    public void setTechnicienAffecte(Technicien technicienAffecte) {
        this.technicienAffecte = technicienAffecte;
    }
    /**
     * Setter sur dt, de type DemandeTravaux
     * @param dt 
     */
    public void setDt(DemandeTravaux dt) {
        this.dt = dt;
    }
    /**
     * Getter sur idAffectation
     * @return 
     */
    public int getIdAffectation() {
        return idAffectation;
    }
    /**
     * Getter sur dateAffectation
     * @return 
     */
    public Date getDateAffectation() {
        return dateAffectation;
    }
    /**
     * Getter sur datePriseEnCharge
     * @return 
     */
    public Date getDatePriseEnCharge() {
        return datePriseEnCharge;
    }
    /**
     * Getter sur technicienAffecte de type Technicien
     * @return 
     */
    public Technicien getTechnicienAffecte() {
        return technicienAffecte;
    }
    /**
     * Getter sur dt de type DemandeTravaux
     * @return 
     */
    public DemandeTravaux getDt() {
        return dt;
    }
    
    
    
}
