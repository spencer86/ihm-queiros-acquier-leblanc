/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

/**
 *
 * @author mhverron
 */
public class Service {
    
    private int idService;
    private String nom;
    private Employe responsable;

    public Service(int idService, String nom) {
        this.idService = idService;
        this.nom = nom;
    }

    public void setResponsable(Employe responsable) {
        this.responsable = responsable;
    }

    public int getIdService() {
        return idService;
    }

    public String getNom() {
        return nom;
    }

    public Employe getResponsable() {
        return responsable;
    }
    
    
}
