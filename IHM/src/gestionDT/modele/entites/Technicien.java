/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.modele.entites;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mhverron
 */
public class Technicien extends Employe {
    
    private List<Affectation> mesAffectations;
    
    

    public Technicien(int idEmploye, String nom, String telBureau, String mail) {
        super(idEmploye, nom, telBureau, mail);
        this.mesAffectations = new ArrayList<>();
    }

    public Technicien(int idEmploye, String nom, String telBureau, String mail, Agence uneAgence, Service unService) {
        super(idEmploye, nom, telBureau, mail, uneAgence, unService);
        this.mesAffectations = new ArrayList<>();
    }

    public List<Affectation> getAffectations() {
        return mesAffectations;
    }
    /**
     * Permet d'ajouter une/des affectations
     * @param uneAffectation 
     */
    public void ajouterAffectation(Affectation uneAffectation){
        this.mesAffectations.add(uneAffectation);
    }
    /**
     * Permet d'enlever une/des affectations
     * @param uneAffectation 
     */
    public void enleverAffectation(Affectation uneAffectation){
        this.mesAffectations.remove(uneAffectation);
    }
    
}
