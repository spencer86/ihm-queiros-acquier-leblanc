package gestionDT;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import gestionDT.controlleur.ControlleurDT;
import gestionDT.vue.AppliGestionDT;

/**
 *
 * @author aqueiros
 */
public class PosterDT {
    public static void main(String[] args)
    {
        ControlleurDT cDT = new ControlleurDT();
        AppliGestionDT vPD = new AppliGestionDT(cDT);
        cDT.setVue(vPD);
        vPD.afficher();
    }
}
