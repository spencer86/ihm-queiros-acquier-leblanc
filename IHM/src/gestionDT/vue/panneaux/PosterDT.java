/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.vue.panneaux;

import gestionDT.controlleur.ControlleurDT;
import gestionDT.modele.entites.TypeDemande;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author aqueiros
 */
public class PosterDT extends javax.swing.JPanel {


    private ControlleurDT cDT;

    public PosterDT(ControlleurDT cDT) {
        initComponents();
        this.cDT = cDT;
    }
    
    public void afficher()
    {
        this.setVisible(true);
    }
    /**
     * Initialise l'employé connecté
     * @param nomEmp 
     */
    public void initEmployeConnecte(String nomEmp)
    {
        this.jLabel2.setText("Bonjour " + nomEmp);
    }
    /**
     * Permet d'effacer les champs
     */
    public void effacerChamps()
    {
        this.jComboBox1.setSelectedIndex(0);
        this.jTextArea1.setText("");
    }
    /**
     * Initialise la comboBox contenant les types de demande
     * @param choix 
     */
    public void initCbTypes(ArrayList<TypeDemande> choix)
    {
        for(TypeDemande s : choix)
        {
            this.jComboBox1.addItem(s);
        }
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * form.
     * WARNING:
     * Do
     * NOT
     * modify
     * this
     * code.
     * The
     * content
     * of
     * this
     * method
     * is
     * always
     * regenerated
     * by
     * the
     * Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAnnuler = new javax.swing.JButton();
        btnPosterDT = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        btnAnnuler.setText("Annuler");
        btnAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnulerActionPerformed(evt);
            }
        });

        btnPosterDT.setText("PosterDT");
        btnPosterDT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPosterDTActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel4.setText("Description DT :");

        jLabel3.setText("Choix type DT :");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        jLabel2.setText("Bonjour");

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel1.setText("Demande de Travaux");
        jLabel1.setAlignmentY(0.0F);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(99, 99, 99)
                        .add(jLabel1))
                    .add(layout.createSequentialGroup()
                        .add(14, 14, 14)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(jLabel3)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jLabel2)
                            .add(jLabel4)
                            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(layout.createSequentialGroup()
                        .add(131, 131, 131)
                        .add(btnPosterDT)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(btnAnnuler)))
                .addContainerGap(63, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel2)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(jComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel4)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 27, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnPosterDT)
                    .add(btnAnnuler))
                .add(14, 14, 14))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnulerActionPerformed
        // TODO add your handling code here:
        this.effacerChamps();
    }//GEN-LAST:event_btnAnnulerActionPerformed

    /**
     * Ecouteur sur btnPosterDT. A la réception du clic sur le bouton, affiche une boîte de dialogue avec les détails de la DT. Si l'on clique sur annuler ou modifier la DT, les champs sont effacés. Sinon, on poste la DT
     * @param evt 
     */
    private void btnPosterDTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPosterDTActionPerformed
        // TODO add your handling code here:
        String td = ((TypeDemande)(this.jComboBox1.getSelectedItem())).getLibelle();
        TypeDemande td2 = (TypeDemande)this.jComboBox1.getSelectedItem();
        String desc = this.jTextArea1.getText();
        Object[] options = {"Annuler DT", "Modifier DT", "Confirmer DT"};
        int n = JOptionPane.showOptionDialog(this, "Recapitulatif DT : \nDemandeur : " + this.jLabel2.getText().substring(7) + "\n" + "TypeDT : " + td2.getLibelle() + "\n" + "Description : " + desc , "Confirmer demande", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
        System.out.println(n);
        if(n == 0 || n == -1)
        this.effacerChamps();
        else if(n == 2)
        {
            this.cDT.posterDT(td2, desc);
        }
    }//GEN-LAST:event_btnPosterDTActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnPosterDT;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
