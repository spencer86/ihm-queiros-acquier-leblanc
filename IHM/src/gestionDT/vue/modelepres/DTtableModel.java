/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.vue.modelepres;

import gestionDT.modele.entites.DemandeTravaux;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author aqueiros
 */
public class DTtableModel extends AbstractTableModel
{
    private List<DemandeTravaux> dTaAfficher;
    private String[] listNomColonne;
    
    public DTtableModel(List<DemandeTravaux> dtAfficher)
    {
        this.dTaAfficher = dtAfficher;
        this.listNomColonne = new String[]{"numDT", "Description", "DateDemande", "Etat", "DatePriseEnCharge"};
    }
    
    /**
     * Retourne le nombre de lignes, et donc de DT.
     * @return 
     */
    @Override
    public int getRowCount() {
        return this.dTaAfficher.size();
    }

    /**
     * Retourne le nombre de colonnes. 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return 5;
    }

    /**
     * Permet de retourne la valeur pour un indexColonne (columnIndex) et un indexLigne(rowIndex)
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex)
        {
            case 0 : return this.dTaAfficher.get(rowIndex).getNumDT();
            case 1 : return this.dTaAfficher.get(rowIndex).getDescription();
            case 2 : return this.dTaAfficher.get(rowIndex).getDateDemande();
            case 3 : return this.dTaAfficher.get(rowIndex).getEtat();
            case 4 : return this.dTaAfficher.get(rowIndex).getDatePriseEnCharge();
        }
        return null;
    }
    
    /**
     * Permet de retourner la classe concernant une colonne selon un index (columnIndex)
     * @param columnIndex
     * @return 
     */
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        switch(columnIndex)
        {
            case 0 : return java.lang.Integer.class;
            case 1 : return java.lang.String.class;
            case 2 : return java.util.Date.class;
            case 3 : return gestionDT.modele.entites.EtatDemande.class;
            case 4 : return java.util.Date.class;    
        }
        return null;
    }
    
    /**
     * Permet de retourner le nom d'une colonne pour un indexColonne (columnIndex)
     * @param columnIndex
     * @return 
     */
    @Override
    public String getColumnName(int columnIndex)
    {
        return this.listNomColonne[columnIndex];
    }
    
}
