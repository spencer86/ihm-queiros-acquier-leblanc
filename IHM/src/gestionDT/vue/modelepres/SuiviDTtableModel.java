/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.vue.modelepres;

import gestionDT.controlleur.ControlleurDT;
import gestionDT.modele.entites.DemandeTravaux;
import gestionDT.modele.entites.TypeDemande;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author nacquier
 */
public class SuiviDTtableModel extends AbstractTableModel {
    private List<DemandeTravaux> listeDT;
    private ControlleurDT ctrl;
    private String[] nomCol;
    
    public SuiviDTtableModel(List<DemandeTravaux> listeDT, ControlleurDT ctrl){
        this.listeDT = listeDT;
        this.ctrl = ctrl;
        this.nomCol = new String[] {"numDT", "Description", "Date Demande", "Etat", "Annulation"};
    }
    
    @Override
    public int getRowCount() {
        return this.listeDT.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    /**
     * @author nacquier
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0: return this.listeDT.get(rowIndex).getNumDT();
            case 1: return this.listeDT.get(rowIndex).getDescription();
            case 2: return this.listeDT.get(rowIndex).getDateDemande();
            case 3: return this.listeDT.get(rowIndex).getEtat();
        }
        return null;
    }
    
    /**
     * @author nacquier
     * @param columnIndex
     * @return 
     */
    @Override
    public Class<?> getColumnClass(int columnIndex){
        switch(columnIndex){
            case 0: return java.lang.Integer.class;
            case 1: return java.lang.String.class;
            case 2: return java.util.Date.class;
            case 3: return gestionDT.modele.entites.EtatDemande.class;
            case 4: return java.lang.Boolean.class;
        }       
        return null;
    }
    
    /**
     * Getter sur la DT sélectionnée
     * @param rowIndex
     * @return 
     */
    public DemandeTravaux getDtSelectionne(int rowIndex)
    {
        return this.listeDT.get(rowIndex);
    }
    
    /**
     * Permet de définir si une cellule est éditable ou non pour un indexColonne/Ligne
     * @author nacquier
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex){
        if(columnIndex == 4){
            return true;
        }
        return false;
    }
    
    /**
     * Demande à l'utilisateur une confirmation de l'annulation de la DT sélectionnée
     * @author nacquier
     * @param aValue
     * @param rowIndex
     * @param columnIndex 
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex){
        String desc = this.listeDT.get(rowIndex).getDescription();
        TypeDemande typeDT = (TypeDemande)this.listeDT.get(rowIndex).getType();
        String[] options = {"Confirmer", "Annuler"};
        int n = JOptionPane.showOptionDialog(null, "Recapitulatif DT : \n Type de la DT : " + typeDT, "Description de la DT : " + typeDT, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        System.out.println(n);
        
        if(n == 0)
        {   
            this.ctrl.annuler(this.listeDT.remove(rowIndex));
        }
    }
    
    /**
     * Getter sur le nom de la colonne
     * @param columnIndex
     * @return 
     */
    @Override
    public String getColumnName(int columnIndex)
    {
        return this.nomCol[columnIndex];
    }

    
}
