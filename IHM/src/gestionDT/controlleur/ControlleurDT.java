/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionDT.controlleur;

import java.util.ArrayList;
import gestionDT.modele.accesBD.EnregistrementDonnees;
import gestionDT.modele.entites.Employe;
import gestionDT.modele.accesBD.RecuperationDonneesInitiales;
import gestionDT.modele.accesBD.RecupererDonneesInitiales;
import gestionDT.modele.entites.DemandeTravaux;
import gestionDT.modele.entites.TypeDemande;
import gestionDT.vue.AppliGestionDT;
import java.util.List;

/**
 *
 * @author aqueiros nacquier
 */
public class ControlleurDT {
    private Employe employe;
    private ArrayList<TypeDemande> listeTypeDT;
    private AppliGestionDT vue;
    private EnregistrementDonnees insert;
    
    public ControlleurDT()
    {
        RecupererDonneesInitiales recupDon = new RecupererDonneesInitiales();
        this.employe = recupDon.recupererEmployeConnecte();
        this.listeTypeDT = (ArrayList<TypeDemande>)recupDon.recupererTypesDemande();
        this.insert = new EnregistrementDonnees();
        recupDon.recupererDemandesPostees(employe);
    }
    /**
     * 
     * @param uneDT 
     */
    public void annuler(DemandeTravaux uneDT){
        this.insert.annulerDT(uneDT);
    }
    /**
     * 
     * @param vue 
     */
    public void setVue(AppliGestionDT vue)
    {
        this.vue = vue;
        this.vue.initEmployeConnecte(this.employe.getNom());
        this.vue.initCbTypes(this.listeTypeDT);
    }
    /**
     * poster DT permet de poster une DT, vérifie d'abord si la description est présente, si oui, on insert (s'il n'y a pas d'erreur d'insert)
     * @param td
     * @param desc 
     */
    public void posterDT(TypeDemande td, String desc)
    {
        if(desc.isEmpty())
            this.vue.afficherErreur("Description manquante");
        else
        {
            int n =this.insert.enregistrerDemandeTravaux(new DemandeTravaux(desc, this.employe,td ));
            if(n <= 0)
            {
                this.vue.afficherErreur("Erreur d'insertion");
            }
            else
            {
                RecupererDonneesInitiales recupDon = new RecupererDonneesInitiales();
                recupDon.recupererDemandesPostees(this.employe);
                this.vue.afficherRecapDT(this.employe.getDT());
            }
        }
    }
     /**
      * Getter sur les DT soumises
      * @return 
      */
    public List<DemandeTravaux> getDTSoumises()
    {
        RecupererDonneesInitiales recupDon = new RecupererDonneesInitiales();
        recupDon.recupererDemandesPostees(this.employe);
        return this.employe.getDT();        
    }
    
    /**
     * 
     * @param dt 
     */
    public void ecranDetail(DemandeTravaux dt)
    {
        this.vue.afficherDetailDT(dt);
    }
    /**
     * 
     */
    public void ecranAvacement()
    {
        this.vue.afficherAvancement();
    }

}
